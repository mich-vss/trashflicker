﻿/*!
	@file /Assets/Scripts/SafeMethods.cs

	@copyright dr.Panda ltd.
	
	@brief
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Homemade class to handle the GetComponent safely
/// </summary>
public static class Safe
{
    /// <summary>
    /// GetComponent safe call, print debug strings and throw an exception if fails
    /// </summary>
    /// <typeparam name="T">Type of the object</typeparam>
    /// <param name="g">The object where to get the component</param>
    /// <param name="c">Object that is calling this function</param>
    /// <returns>The fount component</returns>
    public static T GetComponent<T>(this GameObject g, GameObject c) where T : Component
    {
        if (g == null) // Check if actually g is attached
        {
            Debug.LogError("An object that shoulbe be attached in " + c.gameObject.name + " is NULL");
            throw new MissingComponentException();
        }
        else
            return findComponent<T>(g);
    }

    /// <summary>
    /// GetComponent safe call, print debug strings and throw an exception if fails
    /// </summary>
    /// <typeparam name="T">Type of the object</typeparam>
    /// <param name="g">The object where to get the component</param>
    /// <param name="c">Object that is calling this function</param>
    /// <returns>The fount component</returns>
    public static T GetComponent<T>(this Component g, Component c) where T : Component
    {
        return GetComponent<T>(g.gameObject, c.gameObject);
    }

    /// <summary>
    /// GetComponent safe call, print debug strings and throw an exception if fails
    /// </summary>
    /// <typeparam name="T">Type of the object</typeparam>
    /// <param name="g">The object where to get the component</param>
    /// <returns>The fount component</returns>
    public static T GetComponent<T>(this Component g) where T : Component
    {
        return GetComponent<T>(g.gameObject, g.gameObject);
    }

    /// <summary>
    /// GetComponent safe call, print debug strings and throw an exception if fails
    /// </summary>
    /// <typeparam name="T">Type of the object</typeparam>
    /// <param name="g">The object where to get the component</param>
    /// <param name="c">Object that is calling this function</param>
    /// <returns>The fount component</returns>
    public static T GetComponent<T>(this GameObject g) where T : Component
    {
        return GetComponent<T>(g, g);
    }

    private static T findComponent<T>(GameObject g) where T : Component
    {
        T foundComponent = g.GetComponent<T>(); // Do the get component of g
        if (foundComponent == null)
        {
            Debug.LogError("An object of type " + typeof(T).Name + " is not attached to " + g.name);
            throw new MissingComponentException();
        }
        return foundComponent;
    }

}
