﻿/*!
	@file /Assets/Scripts/MeterGuiObj.cs

	@copyright dr.Panda ltd.
	
	@brief
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

interface IMeterGuiObj
{
    int Value { get; set; }
    void SetActive(bool onOff);
}

/// <summary>
/// Generic object with an integer numeric label on it
/// </summary>
public class MeterGuiObj : MonoBehaviour, IMeterGuiObj
{
    [SerializeField]
    Text NumericLabel = null;

    private int _labelValue = 0;

    /// <summary>
    /// The number displayed on the label
    /// </summary>
    public int Value
    {
        get { return _labelValue; }
        set
        {
            _labelValue = value;
            if (NumericLabel != null)
                NumericLabel.text = _labelValue.ToString();
        }
    }

    /// <summary>
    /// Set the object active and visible
    /// </summary>
    /// <param name="onOff"></param>
    public void SetActive(bool onOff)
    {
        if (NumericLabel != null)
        {
            NumericLabel.gameObject.SetActive(onOff);
            this.gameObject.SetActive(onOff);
        }
    }
}
