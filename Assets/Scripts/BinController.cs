﻿/*!
	@file /Assets/Scripts/BinController.cs

	@copyright dr.Panda ltd.
	
	@brief
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Assertions;


public interface iBinController
{
    void Setup();
    void StartIntroAnimation(float introGap);
    void OpenTop();
    void CloseTop();
    void WaitForTrash();
    void StandStill();
}

public class BinController : MonoBehaviour, iBinController
{

    #region INSPECTORS_FIELDS

    [SerializeField]
    Transform startingPosition = null;

    [SerializeField]
    Transform landingPosition = null;

    [SerializeField]
    uint introMoveTime = 0;

    [SerializeField]
    AudioClip soundClose = null;

    [SerializeField]
    AudioClip soundOpen = null;

    [SerializeField]
    AudioClip soundTrashIn = null;

    #endregion

    #region EVENTS

    /// <summary>
    /// To spread the open animation event
    /// </summary>
    public event onBinOpened OnBinOpened;
    public delegate void onBinOpened(BinController obj);

    /// <summary>
    /// Raised when the intro animation is finished
    /// </summary>
    public event onIntroAnimationFinished OnIntroAnimationFinished;
    public delegate void onIntroAnimationFinished(BinController obj);

    #endregion

    #region PRIVATE_DATA

    /// <summary>
    /// Possible states of the bin animator
    /// </summary>
    private enum binAnimState
    {
        still = 0,
        idle = 1, // Do the idle animation
        introOpen = 2, // Open during the intro
        playOpen = 3 // Open while waiting for trash
    }

    /// <summary>
    /// States of the bin's FSM
    /// </summary>
    private enum binControlState
    {
        none = 0,
        movingToLandingPos = 1, // The bin is moving to its final position
        rotatingToLandingPos = 2, // The bin is rotating according to the final pos point
        waitingForTrash = 3, // The bin is waiting to get trash
    }

    /// <summary>
    /// State of the bin's FSM
    /// </summary>
    private binControlState _currentState = binControlState.none;

    /// <summary>
    /// Animator controller of the bin
    /// </summary>
    private Animator _binAnimator;

    /// <summary>
    /// Time since the bin started to move towards its final position
    /// </summary>
    private float _introMoveElapsedTime = 0.0f;

    /// <summary>
    /// Audio controller attached to the bin
    /// </summary>
    private AudioSource _binAudioController = null;

    #endregion

    #region PUBLIC

    /// <summary>
    /// Stop the bin from moving or doing anything
    /// </summary>
    public void StandStill()
    {
        _currentState = binControlState.none;
        ChangeAnimState(binAnimState.still);
    }

    /// <summary>
    /// Start the intro animation, the bin walks from StartingBinPos to its final position. The bin line up facing 0,0
    /// </summary>
    /// <param name="introGap">Distance from the starting position, so from a bin to another during the intro animation</param>
    public void StartIntroAnimation(float introGap)
    {
        // doing some nasty math here, I could have used just a Ray but I beleive this is more efficent ¯\_(ツ)_/¯ 
        float pointAngle = Mathf.Atan2(startingPosition.position.x, startingPosition.position.z); // Get the angle α between the origin(0,0) and the starting point [α=arctan((y1-y0)/(x1-x0))]
        float distance = Vector3.Distance(startingPosition.position, Vector3.zero) + introGap; // Get the distance from the origin and add the gap
        transform.position = new Vector3((distance * Mathf.Sin(pointAngle)), 0, (distance * Mathf.Cos(pointAngle))); // Given the distance and the angle, calculate the coordinates [px=distance*sin(α), ...]

        Vector3 rotAngle = new Vector3(0, pointAngle * 180 / Mathf.PI, 0); //Get the rotation to face the center of the screen
        transform.rotation = Quaternion.Euler(rotAngle);

        ChangeAnimState(binAnimState.idle); // Start the idle animation
        _currentState = binControlState.movingToLandingPos; // And the current state to move
        _introMoveElapsedTime = 0;
    }

    /// <summary>
    /// Open the bin
    /// </summary>
    public void OpenTop()
    {
        // If it's waiting for trash play the sound
        if (_currentState == binControlState.waitingForTrash)
        {
            ChangeAnimState(binAnimState.playOpen); // Start the open animation
            _binAudioController.clip = soundOpen;
            _binAudioController.Play();
        }
        else
            ChangeAnimState(binAnimState.introOpen);
    }

    /// <summary>
    /// Close the bin
    /// </summary>
    public void CloseTop()
    {
        ChangeAnimState(binAnimState.idle); // Go to idle animation
        // If it's waiting for trash play the sound
        if (_currentState == binControlState.waitingForTrash)
        {
            _binAudioController.clip = soundClose;
            _binAudioController.Play();
        }
    }

    /// <summary>
    /// Set the bin ready to wait for the trash
    /// </summary>
    public void WaitForTrash()
    {
        // If the animator state is not in idle, set it
        if (_binAnimator != null)
        {
            if (_binAnimator.GetInteger("binAnimState") != (int)binAnimState.idle)
                ChangeAnimState(binAnimState.idle);
        }

        // If the bin is not in place, set it
        if (Vector3.Distance(transform.position, landingPosition.position) > 0.1f)
        {
            gameObject.transform.position = landingPosition.position;
            transform.rotation = landingPosition.rotation;
        }

        // Change the FSM state
        _currentState = binControlState.waitingForTrash;
    }

    #endregion

    #region SUBBED_EVENTS

    /// <summary>
    /// Function called by the event created into "Bin_Rig_Open" animation
    /// </summary>
    void BinOpenEvent()
    {
        OnBinOpened(this);
    }

    /// <summary>
    /// A piece of trash falled inside, play the sound
    /// </summary>
    /// <param name="bin"></param>
    /// <param name="trashObj"></param>
    void Event_TrashInside(BinController bin, GameObject trashObj)
    {
        _binAudioController.clip = soundTrashIn;
        _binAudioController.Play();
    }

    #endregion

    /// <summary>
    /// Change the animation state variable inside the bin animator
    /// </summary>
    /// <param name="newstate">the new binAnimState you wanna set</param>
    private void ChangeAnimState(binAnimState newstate)
    {
        if (_binAnimator != null)
            _binAnimator.SetInteger("binAnimState", (int)newstate);
    }

    /// <summary>
    /// Initialize the object
    /// </summary>
    public void Setup()
    {
        // Subcribe to the trash detection event
        BinTrashHitDetection tc = gameObject.GetComponentInChildren<BinTrashHitDetection>();
        if (tc != null)
            tc.OnTrashPlaced += Event_TrashInside;

        // Get the components we need
        _binAnimator = Safe.GetComponent<Animator>(this); 
        _binAudioController = Safe.GetComponent<AudioSource>(this);

        ChangeAnimState(binAnimState.still); // Animation state machine entry state
        transform.position = startingPosition.position; // Place the bin on its starting position
    }

    /// <summary>
    /// Contains the bin controller's FSM
    /// </summary>
    void Update()
    {
        switch(_currentState)
        {
            case binControlState.none:
                {
                    //the bin does nothing...
                    break;
                }
            case binControlState.movingToLandingPos:
                {
                    // lerping to the landing position
                    _introMoveElapsedTime += Time.time;
                    transform.position = Vector3.Lerp(transform.position, landingPosition.position, _introMoveElapsedTime / introMoveTime);

                    // Check if it's in place TODO: check performace
                    if (Vector3.Distance(transform.position, landingPosition.position) < 0.1f)
                        _currentState = binControlState.rotatingToLandingPos; // Start the rotation

                    break;
                }
            case binControlState.rotatingToLandingPos:
                {
                    // lerping to the final rotation position
                    transform.localRotation = Quaternion.Lerp(transform.localRotation, landingPosition.rotation, 3 * Time.deltaTime);

                    // Check if it's fully rotated
                    if (transform.rotation == landingPosition.rotation)
                    {
                        _currentState = binControlState.none;
                        OnIntroAnimationFinished(this); // Tell everyone we're landed
                    }
                    break;
                }
            case binControlState.waitingForTrash:
                {
                    // Do something while waiting for trash?...
                    break;
                }
            default:
                {
                    Debug.LogWarning("Unknown binControl state");
                    break;
                }
        }
    }


}
