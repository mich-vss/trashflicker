﻿/*!
	@file /Assets/Scripts/GameController.cs

	@copyright dr.Panda ltd.
	
	@brief
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

/// <summary>
/// 
/// </summary>
public class GameController: MonoBehaviour
{

    #region INSPECTORS_FIELDS

    [SerializeField]
    List<GameObject> trashPieces = null;

    [SerializeField]
    List<GameObject> trashBins = null;

    [SerializeField]
    float binIntroSpawnGap = 3;

    [SerializeField]
    Transform trashSpawnPosition;

    [SerializeField]
    Image introSplashImage = null;

    [SerializeField]
    Image gameTimerMeter = null;

    [SerializeField]
    Image gameScoreMeter = null;

    [SerializeField]
    Button gameRestartButton = null;

    [SerializeField]
    Image gameOverBox = null;

    [SerializeField]
    Text gameFinalScore = null;

    [SerializeField]
    AudioClip soundBinsMoving = null;

    /// <summary>
    /// Numer of seconds to countdown
    /// </summary>
    [SerializeField]
    int gameTimeSeconds = 90;
    #endregion


    #region PRIVATE_DATA

    enum gameState
    {
        none = 0,
        state_intro = 1,    // Phase 1: Animation intro
        state_play = 2,     // Phase 2: Game play
        state_end = 3       // Phase 3: End screen, recap
    }

    /// <summary>
    /// List of the bin's controller sripts into the scene
    /// </summary>
    private List<BinController> _binControllers;

    /// <summary>
    /// The variable governing the game FSM
    /// </summary>
    private gameState _gameState = gameState.none;

    /// <summary>
    /// It resets on each state change
    /// </summary>
    private bool _stateFirstIn = true;

    /// <summary>
    /// Counts the number of the bins arrived at destination during the intro animation
    /// </summary>
    private int _intro_binsAtDestination = 0;

    /// <summary>
    /// Countdown timer object 
    /// </summary>
    private CountdownTimer _gameTimer;

    /// <summary>
    /// Score object
    /// </summary>
    private MeterGuiObj _gameScore;

    /// <summary>
    /// Piece(s) of trash on the ground
    /// </summary>
    private List<GameObject> _trashOnTheGound;

    /// <summary>
    /// Audio source of the game controller
    /// </summary>
    private AudioSource _gameAudioControl;
    #endregion

    /// <summary>
    /// Here I get all the components we need and I initialize them
    /// </summary>
    void Start()
    {
        Random.InitState(System.Environment.TickCount); //Inizialize RNG

        // Init lists
        _trashOnTheGound = new List<GameObject>();
        _binControllers = new List<BinController>();

        string notAtt = " is not attached to " + gameObject.name; // Convenient debug string

        _gameTimer = Safe.GetComponent<CountdownTimer>(gameTimerMeter, this); // Get the timer
        if (_gameTimer != null)
        {
            _gameTimer.OnTimeOver += Event_TimeOver;
            _gameTimer.SetActive(false);
        }

        _gameScore = Safe.GetComponent<MeterGuiObj>(gameScoreMeter, this); // Get the score meter
        if (gameScoreMeter != null)
            _gameScore.SetActive(false);

        if (gameOverBox != null)
            gameOverBox.gameObject.SetActive(false);
        else
            Debug.LogError("Game Over box" + notAtt);

        if (gameRestartButton != null)
            gameRestartButton.onClick.AddListener(Game_Reset); // Subscribe to the restart button event
        else
            Debug.LogError("Restart Button" + notAtt);

        _gameAudioControl = Safe.GetComponent<AudioSource>(this, this); // Get the audio source

        if (trashSpawnPosition == null)
        {
            trashSpawnPosition = new GameObject().transform; // If there's no spawn position
            trashSpawnPosition.transform.position = new Vector3(0, 1, 0); // set one.
            Debug.LogWarning("Trash spawn position" + notAtt);
        }

        // Fill the list of BinController(s)
        foreach (GameObject go in trashBins)
        {
            BinController toAdd = Safe.GetComponent<BinController>(go, this.gameObject);
            _binControllers.Add(toAdd);
        }

        // Subscribe to the bin events
        for (byte i = 0; i < _binControllers.Count; i++)
        {
            _binControllers[i].Setup(); // Initialize the bin object
            _binControllers[i].OnBinOpened += Event_BinOpened;
            _binControllers[i].OnIntroAnimationFinished += Event_BinAtDestination;
            BinTrashHitDetection tc = _binControllers[i].GetComponentInChildren<BinTrashHitDetection>();
            if (tc != null)
                tc.OnTrashPlaced += Event_TrashPlaced;
        }

        Game_StateChange(gameState.state_intro); // Change the state to begin the game
        //Game_StateChange(gameState.state_play); // Uncomment this to skip the intro animation
    }

    #region GAME_FUNCTIONS

    /// <summary>
    /// Changes the state of the game controller FSM
    /// </summary>
    /// <param name="newState">The new state to set</param>
    void Game_StateChange(gameState newState)
    {
        _stateFirstIn = true;
        _gameState = newState;
    }

    /// <summary>
    /// Clean the ground from any remainig trash
    /// </summary>
    void Game_CleanFieldFromTrash()
    {
        int numTrashToClean = _trashOnTheGound.Count;
        for (int i = 0; i < numTrashToClean; i++)
            Trash_RemoveFromField(_trashOnTheGound[i]);
    }

    /// <summary>
    /// Resets the game to the play state
    /// </summary>
    private void Game_Reset()
    {
        Game_StateChange(gameState.state_play);
    }

    #endregion

    #region SUBBED_EVENTS

    /// <summary>
    /// Event received when the bin has arrived at destination during the intro animation
    /// </summary>
    /// <param name="obj"></param>
    void Event_BinAtDestination(BinController obj)
    {
        _gameAudioControl.Stop();
        _intro_binsAtDestination++;
        if (_intro_binsAtDestination == _binControllers.Count) // If all the bins are arrived
            _binControllers[0].OpenTop(); // Open the first one, so the others at Event_BinOpened
    }

    /// <summary>
    /// Spawn a new piece(s) of trash on the ground at trashSpawnPosition
    /// </summary>
    /// <param name="howMany">Numer of pieces to spawn</param>
    void Trash_SpawnNew(int howMany)
    {
        for(int i=0; i<howMany; i++)
            _trashOnTheGound.Add(Instantiate(trashPieces[Random.Range(0, trashPieces.Count - 1)], trashSpawnPosition));
    }

    /// <summary>
    /// Remove a piece of thrash from the gorund
    /// </summary>
    /// <param name="trashObj">The trash object</param>
    void Trash_RemoveFromField(GameObject trashObj)
    {
        GameObject toRemove = _trashOnTheGound.Find(x => x.gameObject == trashObj); // Find it and save the reference
        if (toRemove)
        {
            _trashOnTheGound.Remove(trashObj); // Remove it from the list
            Destroy(toRemove); // Destroy it from the referece
        }
    }

    /// <summary>
    /// When a trash is placed remove it and increment the score, so spawn a new one
    /// </summary>
    /// <param name="bin"></param>
    /// <param name="trashObj"></param>
    void Event_TrashPlaced(BinController bin, GameObject trashObj)
    {
        Trash_RemoveFromField(trashObj); // Remove the piece of trash
        _gameScore.Value++; // Give one point
        Trash_SpawnNew(1); // Spawn the new one
    }

    /// <summary>
    /// The timer is 0, change the state to state_end
    /// </summary>
    void Event_TimeOver()
    {
        Game_StateChange(gameState.state_end);
    }

    /// <summary>
    /// The bin top is opened, during the intro open the next one
    /// </summary>
    /// <param name="obj"></param>
    void Event_BinOpened(BinController obj)
    {
        if (_gameState == gameState.state_intro) // during the intro animation
        {
            obj.CloseTop();
            int binIndex = _binControllers.IndexOf(obj) +1; // find the index of the next bin to open
            if (binIndex < _binControllers.Count) // check if it's the last
                _binControllers[binIndex].OpenTop();
            else
                Game_StateChange(gameState.state_play);
        }
    }

    #endregion

    /// <summary>
    /// Game's state machine
    /// </summary>
    void Update()
    {
        switch(_gameState)
        {
            case gameState.none:
                {
                    // does nothing..
                    break;
                }
                
            case gameState.state_intro:
                {
                    if (_stateFirstIn)
                    {
                        _stateFirstIn = false;

                        if (soundBinsMoving != null)
                            _gameAudioControl.clip = soundBinsMoving; // Set the intro sound
                        _gameAudioControl.loop = true;
                        _gameAudioControl.volume = 0.5f;
                        _gameAudioControl.PlayDelayed(0.5f); //Play it

                        for (byte i = 0; i < _binControllers.Count; i++)
                            _binControllers[i].StartIntroAnimation(binIntroSpawnGap * i); // The starting distance between each bin is the gap * #bin
                    }
                    break;
                }

            case gameState.state_play:
                {
                    if (_stateFirstIn)
                    {
                        _stateFirstIn = false;

                        Game_CleanFieldFromTrash(); // Just in case, clean the field

                        // Close the splash image and the gameover box
                        introSplashImage.gameObject.SetActive(false); 
                        gameOverBox.gameObject.SetActive(false);

                        // Activate timer and score obj and initialize them
                        _gameTimer.SetActive(true);
                        _gameScore.SetActive(true);
                        _gameScore.Value = 0;
                        _gameTimer.timerValue = gameTimeSeconds;
                        _gameTimer.TimerStart();

                        _binControllers.ForEach(bc => bc.WaitForTrash()); // Set the bins on the right state
                        Trash_SpawnNew(1); // Spawn the first trash
                    }
                    break;
                }

            case gameState.state_end:
                {
                    if (_stateFirstIn)
                    {
                        _stateFirstIn = false;

                        Game_CleanFieldFromTrash(); // Clean the field

                        foreach (BinController b in _binControllers)
                            b.StandStill(); // Stop the bins from moving 
                        // Disable the timer and score
                        _gameTimer.SetActive(false);
                        _gameScore.SetActive(false);

                        // Prepare the gameover box
                        gameRestartButton.gameObject.SetActive(true);
                        gameOverBox.gameObject.SetActive(true);
                        gameFinalScore.text = _gameScore.Value.ToString();
                    }
                    break;
                }
            default:
                {
                    Debug.LogWarning("Unknown game state");
                    break;
                }
        }
    }
}
