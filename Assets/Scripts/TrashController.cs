﻿/*!
	@file /Assets/Scripts/TrashController.cs

	@copyright dr.Panda ltd.
	
	@brief
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// TODO: make a generic class for draggable objects, make the touch radius and the object shift as variables.
/// TODO: Use onMouseDrag?
/// </summary>
public class TrashController : MonoBehaviour
{
    #region INSPECTOR_FIELDS

    public string targetBinName; // Not serialized cuz it's also used in BinTrashHitDetection

    [SerializeField]
    AudioClip soundPickUp = null;

    #endregion

    #region PRIVATE_DATA

    /// <summary>
    /// Audio controller of the trash
    /// </summary>
    private AudioSource _trashAudioControl;

    /// <summary>
    /// Height of the bin
    /// </summary>
    private float _targetBinHeight = 2.5f; //TODO: get the actual bin height in space

    /// <summary>
    /// Target bin object
    /// </summary>
    private BinController _targetBinObj = null;

    /// <summary>
    /// They're dragging this
    /// </summary>
    private bool _isDragging = false;

    #endregion

    /// <summary>
    /// Pick up the object
    /// </summary>
    private void PickUp()
    {
        _isDragging = true;
        gameObject.GetComponent<Rigidbody>().useGravity = false;
        gameObject.GetComponent<Rigidbody>().isKinematic = true;
        _trashAudioControl.Play();
        if (_targetBinObj != null)
            _targetBinObj.OpenTop();
    }

    /// <summary>
    /// Drop the object
    /// </summary>
    private void DropIt()
    {
        _isDragging = false;
        gameObject.GetComponent<Rigidbody>().useGravity = true;
        gameObject.GetComponent<Rigidbody>().isKinematic = false;
        if (_targetBinObj != null)
            _targetBinObj.CloseTop();
    }

    /// <summary>
    /// Dragging the object
    /// </summary>
    private void Dragging()
    {
        //Set the plane at the same height of the target
        Plane plane = new Plane(Vector3.up, new Vector3(0, _targetBinHeight, 0));
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition); 
        float distance;
        if (plane.Raycast(ray, out distance))// Shoot the ray
        {
            Vector3 nPos = ray.GetPoint(distance) + new Vector3(0, 0, 1.25f); // shift a little bit up so the player doesn't cover the piece with its finger
            transform.position = nPos;
        }
    }

    // Start is called before the first frame update
    void Start()
    {
        _targetBinObj = Safe.GetComponent<BinController>(GameObject.Find(targetBinName));
        _trashAudioControl = Safe.GetComponent<AudioSource>(this, this);
        if (soundPickUp != null)
            _trashAudioControl.clip = soundPickUp;

    }


    void Update()
    {
        if (Input.GetMouseButton(0) && _isDragging)
            Dragging();
        else if (Input.GetMouseButtonUp(0) && _isDragging)
            DropIt();

        if (Input.GetMouseButtonDown(0))
        {
            // Use a spherical ray so it's easier to pick up the trash with fingers
            RaycastHit[] hits;
            hits = Physics.SphereCastAll(Camera.main.ScreenPointToRay(Input.mousePosition), 1.0f, 50);
            // Check all the objects hit by the ray
            for (int i = 0; i < hits.Length; i++)
            {
                if (hits[i].transform.gameObject == this.gameObject)
                {
                    PickUp();
                    break;
                }
            }

        }
    }



}
