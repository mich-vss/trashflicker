﻿/*!
	@file /Assets/Scripts/BinTrashHitDetection.cs

	@copyright dr.Panda ltd.
	
	@brief
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BinTrashHitDetection : MonoBehaviour
{
    /// <summary>
    /// A trash object is placed into the right bin
    /// </summary>
    public event onTrashPlaced OnTrashPlaced;
    public delegate void onTrashPlaced(BinController bin, GameObject trashObj);

    /// <summary>
    /// The bin that contains the detection box
    /// </summary>
    private BinController _parentBin;

    void Start()
    {
        // Get reference
        _parentBin = gameObject.GetComponentInParent<BinController>();
        if (_parentBin == null)
            Debug.LogWarning("BinController not attached to " + _parentBin.gameObject.name);
    }

    private void OnTriggerEnter(Collider other)
    {
        // Check if we the collided object is a piece of trash
        TrashController tc = other.gameObject.GetComponent<TrashController>(); 
        if (tc !=null && _parentBin != null)
        {
            // Is this the right bin?
            if(tc.targetBinName == _parentBin.gameObject.name)
                OnTrashPlaced(_parentBin, other.gameObject);
        }
    }

}
