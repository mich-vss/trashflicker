﻿/*!
	@file /Assets/Scripts/CountDownTimer.cs

	@copyright dr.Panda ltd.
	
	@brief
 */

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

interface ICountdownTimer
{
    void TimerStart();
    int timerValue { get; set; }
    int currentTime { get; }
}

/// <summary>
/// Generic class for a countdown timer
/// </summary>
public class CountdownTimer : MeterGuiObj, ICountdownTimer
{
    /// <summary>
    /// When the countdown timer reaches 0 this event is triggered
    /// </summary>
    public event onTimeOver OnTimeOver;
    public delegate void onTimeOver();

    /// <summary>
    /// The timer is running
    /// </summary>
    private bool _isRunning = false;

    private int _timerValue = 0;
    private float _currentTime = 0;

    /// <summary>
    /// Timer's starting time in seconds
    /// </summary>
    public int timerValue
    {
        get { return _timerValue; }
        set { _timerValue = value; }
    }

    /// <summary>
    /// Time left in seconds
    /// </summary>
    public int currentTime
    {
        get { return (int)_currentTime; }
    }

    /// <summary>
    /// Start the timer to countdown
    /// </summary>
    public void TimerStart()
    {
        _isRunning = true;
        _currentTime = _timerValue;
    }

    void Update()
    {
        if (_isRunning)
        {
            _currentTime -= Time.deltaTime;
            this.Value=(int)_currentTime; // Set the time in the label
            if (_currentTime <= 0)
            {
                _currentTime = 0;
                _isRunning = false;
                OnTimeOver(); // Trigger the event
            }
        }
    }
}
