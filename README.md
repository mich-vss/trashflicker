﻿Trash Flicker

Implementation notes:

- Made with Unity 2019.1.5f1

- Use the positioning objects inside "Positions" from the Hierarchy to change the bins position and the spawn points

- You can add more bins or more trash pieces just by adding them to the GameController's list

- I've implemented a state machine for the bins script and the GameController

- Please check for the TODO tags in the code

- The game has been tested on an android device

- The game mechanics doesn't require for multitouch, anyway the code altready allows to have multiple piece of trash on the ground

To be improved:

- The Trashbin moving sound is not nice, especially during the intro, better to use a music. Therefore, the intro moving sound is not assigned and the open/close sounds are muted on purpose during the intro
- The perspective while dropping the trash can be improved
- World walls, drop prevention
- Some abstraction of the game elements can be made
